// Your code goes here
//1
function getAge(dob) {
    let now = new Date();
    let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    let dobnow = new Date(today.getFullYear(), dob.getMonth(), dob.getDate());
    let age;
    age = today.getFullYear() - dob.getFullYear();
    if (today < dobnow) {
        age = age - 1;
    }
    return age;
}
//2
Date.longDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function long_Days(dt) {
    return Date.longDays[dt.getDay()];
}
//3
    Date.longDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
let monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'];

function getProgrammersDay(dt) {
    let number = 255;
    let date = new Date(dt, '00', '01');
    date.setDate(date.getDate() + number);
    let day = date.getDate();
    let monthIndex = date.getMonth();
    let year = date.getFullYear();
    return day + ' ' + monthNames[monthIndex] + ', ' + year + '(' + Date.longDays[date.getDay()] + ')';
}
// 4
Date.longDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function howFarIs(dt) {
    dt = dt[0].toUpperCase() + dt.substring(1);
    let now = new Date();
    let date = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    let today = Date.longDays[date.getDay()];
    let n = Date.longDays.indexOf(dt);
    let nNow = Date.longDays.indexOf(today);
    let res = Math.abs(nNow - n);
    if (res === 0) {
        return 'Hey, today is ' + dt + ' =)';
    } else {
        return "It's " + res + ' day(s) left till ' + dt + '.';
    }
}
howFarIs('friday');
//5
function isValidIdentifier(name) {
    let validName = /^[$A-Z_][0-9A-Z_$]*$/i;
    return validName.test(name);
}
//6
function capitalize(str) {
    return str.replace(/( |^)[a-z]/g, function(x) {
        return x.toUpperCase();
    });
}
const testStr = 'My name is John Smith. I am 27.';
console.log(capitalize(testStr));
//7
function isValidAudioFile(name) {
    let validName = /\.(?:flac|mp3|alac|aac)\.([a-z])|([A-Z])/i
    return validName.test(name);
}
// 8
function getHexadecimalColors(str) {
    let regexp = /#([a-f0-9]{3}){1,2}\b/gi;
    return str.match(regexp); // #3f3 #AA00ef #abc
}
const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';
console.log(getHexadecimalColors(testString)); // ["#3f3", "#AA00ef"]
console.log(getHexadecimalColors('red and #0000')); // [];
// 9
function isValidPassword(str) {
    let regexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8}/g;
    return regexp.test(str);
}
//10
function addThousandsSeparators(str) {
    return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}