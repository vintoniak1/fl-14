const two = 2,
    three = 3,
    four = 4,
    five = 5,
    six = 6,
    seven = 7,
    nine = 9;

console.log('Insertion sort');

function insertionSort(arr) {
    let i, len = arr.length,
        el, j;
    for (i = 0; i < len; i++) {
        el = arr[i];
        j = i - 1;
        while ((j > -1) && (el < arr[j])) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = el;
    }
    return arr;
}


console.log(insertionSort([seven, five, two, four, three, nine])); //[2, 3, 4, 5, 7, 9]
console.log(insertionSort([nine, seven, five, four, three, 1])); //[1, 3, 4, 5, 7, 9]
console.log(insertionSort([1, two, three, four, five, six])); //[1, 2, 3, 4, 5, 6]

