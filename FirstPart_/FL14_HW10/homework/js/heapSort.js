const two = 2,
    three = 3,
    four = 4,
    five = 5,
    six = 6,
    seven = 7,
    nine = 9;
console.log('Heap sort');

function heapSort(arr) {
    createMaxHeap(arr);
    let endIndex = arr.length - 1;
    while (endIndex > 0) {
        swap(arr, 0, endIndex);
        heapify(arr, 0, endIndex);
        endIndex--;
    }
    return arr;
}

function createMaxHeap(arr) {
    let startIndex = Math.floor(arr.length / two);
    while (startIndex >= 0) {
        heapify(arr, startIndex, arr.length)
        startIndex--;
    }
}

function heapify(array, index, maxIndex) {
    while (index < maxIndex) {
        let leftChildIndex = two * index + 1;
        let rightChildIndex = two * index + two;

        let largestValueIndex = index;
        if (leftChildIndex < maxIndex && array[leftChildIndex] > array[largestValueIndex]) {
            largestValueIndex = leftChildIndex;
        }
        if (rightChildIndex < maxIndex && array[rightChildIndex] > array[largestValueIndex]) {
            largestValueIndex = rightChildIndex;
        }
        if (index === largestValueIndex) {
            return;
        }
        swap(array, index, largestValueIndex);
        index = largestValueIndex;
    }
}

function swap(arr, i, j) {
    let temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}