const two = 2,
    three = 3,
    four = 4,
    five = 5,
    six = 6,
    seven = 7,
    nine = 9;
console.log('Bubble sort');

function bubbleSort(arr) {
    let len = arr.length;
    for (let i = 0; i < len; i++) {
        for (let j = 0; j < len; j++) {
            if (arr[j] > arr[j + 1]) {
                let temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    return arr;
}

console.log(bubbleSort([seven, five, two, four, three, nine])); //[2, 3, 4, 5, 7, 9]
console.log(bubbleSort([nine, seven, five, four, three, 1])); //[1, 3, 4, 5, 7, 9]
console.log(bubbleSort([1, two, three, four, five, six])); //[1, 2, 3, 4, 5, 6]