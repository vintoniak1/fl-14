const two = 2,
    three = 3,
    four = 4,
    five = 5,
    six = 6,
    seven = 7,
    nine = 9;
console.log('Selection sort');

function selectionSort(arr) {
    let minIdx, temp,
        len = arr.length;
    for (let i = 0; i < len; i++) {
        minIdx = i;
        for (let j = i; j < len; j++) {
            if (arr[j] < arr[minIdx]) {
                minIdx = j;
            }
        }
        temp = arr[i];
        arr[i] = arr[minIdx];
        arr[minIdx] = temp;
    }
    return arr;
}

console.log(selectionSort([seven, five, two, four, three, nine])); //[2, 3, 4, 5, 7, 9]
console.log(selectionSort([nine, seven, five, four, three, 1])); //[1, 3, 4, 5, 7, 9]
console.log(selectionSort([1, two, three, four, five, six])); //[1, 2, 3, 4, 5, 6]