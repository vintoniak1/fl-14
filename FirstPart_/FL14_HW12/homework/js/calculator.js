//your code goes here

function calculate(expression) {
    if(Array.isArray(expression)) {
 expression = expression.join(''); 
}
    let allowedChars = '1234567890%^*()-+/. ';
    for (let i = 0; i < expression.length; i++) {
        if (allowedChars.indexOf(expression.charAt(i)) < 0) {
 throw new Error("Invalid expression: unexpected '" + expression.charAt(i) + "' in '"+expression+"'"); 
}
    }
    let a = eval(expression);
    if ( Number.isNaN(a) ) {
        throw new Error('Invalid expression - NaN');
    }

    return eval(expression);
}

try{
    let expr = prompt('Enter the expression, please');
    alert(calculate(expr));

}catch(e){
    alert(e.message);
}
