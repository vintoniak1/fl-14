function visitLink(path) {
    //your code goes here
    if (path === 'Page1') {
        let n = localStorage.getItem('Page1');
        if (n === null) {
            n = 0;
        }
        n++;
        localStorage.setItem('Page1', n);
    }
    if (path === 'Page2') {
        let n = localStorage.getItem('Page2');
        if (n === null) {
            n = 0;
        }
        n++;
        localStorage.setItem('Page2', n);
    }
    if (path === 'Page3') {
        let n = localStorage.getItem('Page3');
        if (n === null) {
            n = 0;
        }
        n++;
        localStorage.setItem('Page3', n);
    }
}

function viewResults() {
    let n1 = localStorage.getItem('Page1');
    let n2 = localStorage.getItem('Page2');
    let n3 = localStorage.getItem('Page3');
    if (n1 === null) {
        n1 = 0;
    }
    if (n2 === null) {
        n2 = 0;
    }
    if (n3 === null) {
        n3 = 0;
    }
    document.write('Your visited Page1 ' + n1 + ' time(s)');
    document.write('Your visited Page1 ' + n2 + ' time(s)');
    document.write('Your visited Page1 ' + n3 + ' time(s)');
    localStorage.clear();
}