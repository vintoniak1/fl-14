// Your code goes here
let n = prompt('Enter amount of battaries');
let p = prompt('Enter percentage of defective batteries');
if (n < 0 || (p > 100 || p < 0) || isNaN(n)) {
    alert("Invalid input data");
} else {
    let bad_n = 0.1 * n;
    let good_n = n - bad_n;
    alert("Amount of batteries:" + n + "\nDefective rate:" + p + "\nAmount of defective batteries: " + bad_n + "\nAmount of working batteries: " + good_n);
}