// Your code goes here
//1

function isEqual(a,b) {
    return Object.is(a, b);
}



//2

function numberToString(a) {
    return a.toString();
}


//3

function storeNames() {
    let arr = [];
    for (let i = 0; i < arguments.length; i++) {
        arr.push(arguments[i]);
    }
    return arr;
}


//4

function getDivision(a,b) {
    let result;
    if (b > a) {
        result = b / a;
    } else {
        result = a / b;
    }
    return result;
}

//5

function negativeCount(arr) {
    let k = 0;
    for ( let i = 0; i < arr.length; i++) {
        if (arr[i] < 0 ) {
            k++;
        }
    }
    return k;
}


//6

function letterCount(a, b) {
    let res = 0, index = 0;
    while((index = a.indexOf(b)) >= 0) {
        a = a.substring(index + b.length);
        res++;
    }
    return res;
}

//7

function countPoints(arr) {
    let three = 3;
    let score1, score2;
    let k = 0;
    for (let i = 0; i < arr.length; i++) {

        let arr_little =arr[i].split(':');
        score1 = parseInt(arr_little[0]);
        score2 = parseInt(arr_little[1]);

        if (score1 > score2) {
            k +=three;
        }
        if (score1 === score2) {
            k ++;
        }
    }
    return k;

}












