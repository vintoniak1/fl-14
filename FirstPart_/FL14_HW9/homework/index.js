const two = 2, three = 3, minusTwo = -2, eight = 8, five = 5, four = 4, seven = 7, oneYear = 365, thisYear = 2020;
// 1.
let array = ['1', '2', '3', '4', '5'];
let arrayResult = [];
if (typeof array[0] === 'string') {

    arrayResult = array.map(Number);
}
if (typeof array[0] === 'number') {
    arrayResult = array.map(String);
}
console.log(arrayResult);


// 2
let arr = [1,two,three];

function executeforEach(arr) {
    arr.forEach(function (el) {
        console.log(el * two);
    });
}
executeforEach(arr);


// 3


let arr1 = [two,'5',eight];

function mapArray(arr1) {
    arr1.forEach(function (el) {
        if (typeof el==='string') {
            el = parseInt(el);
        }
        console.log(el + three);
    });
}
mapArray(arr1);


//4

let arr2 = [two,five,eight];

function filterArray(arr2) {
    arr2.forEach(function (el) {
        if (el % two === 0 ) {
            console.log(el);
        }

    });
}
filterArray(arr2);


// 5
let a = [1,two,four,five];
let b = 4;
function getValuePosition(a, b) {
    let c = a.indexOf(b) +1;
    console.log(c);
} // returns 3
getValuePosition(a,b);



// 6

function flipOver(a) {
    let array = a.split('');
    array.reverse()
    console.log( array.join('') );
}
flipOver('hey world');


// 7


function makeListFromRange(start, end) {
    let len = end - start + 1;
    let a = new Array(len);
    for (let i = 0; i < len; i++) {
        a[i] = start + i;
    }
    console.log(a);
}
makeListFromRange(two,seven);



// 8
const fruits = [
    { name: 'apple', weight: 0.5 },
    { name: 'pineapple', weight: 2 }
];

function getArrayOfKeys(fruits){

    fruits.forEach(function (element) {
        console.log(element.name);
    });
}
getArrayOfKeys(fruits);




// 9

const basket = [
    { name: 'Bread', weight: 0.3 },
    { name: 'Coca-Cola', weight: 0.5 },
    { name: 'Watermelon', weight: 8 }
];


function getTotalWeight(basket){
    let sum = 0;
    basket.forEach(function (element) {
        sum += element.weight;
    });
    console.log(sum);
}
getTotalWeight(basket);

//10


const date1 = new Date(thisYear, 0, two);

function getPastDay(date,days) {
    date.setDate(date.getDate() - days);
    console.log(date);
}

getPastDay(date1, 1); // 1, (1 Jan 2020)
getPastDay(date1, two); // 31, (31 Dec 2019)
getPastDay(date1, oneYear); // 2, (2 Jan 2019)

//11

function formatDate(date) {
    let newDate = date.getFullYear() + '/' + ('0' + (date.getMonth() + 1)).slice(minusTwo) +
        '/' + ('0' + date.getDate()).slice(minusTwo) +
        ' ' + ('0' + date.getHours()).slice(minusTwo) + ':' + ('0' + date.getMinutes()).slice(minusTwo);
    console.log(newDate);
}

formatDate(new Date('6/15/2019 09:15:00')) // "2018/06/15 09:15"
formatDate(new Date()) // "2020/04/07 12:56"


